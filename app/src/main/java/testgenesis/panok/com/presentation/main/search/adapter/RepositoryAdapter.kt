package testgenesis.panok.com.presentation.main.search.adapter

import testgenesis.panok.com.domain.error.ErrorUI
import testgenesis.panok.com.presentation.common.adapter.AdapterKeys
import testgenesis.panok.com.presentation.common.adapter.BaseAdapter
import testgenesis.panok.com.presentation.common.adapter.ViewType
import testgenesis.panok.com.presentation.common.adapter.items.ErrorItem
import testgenesis.panok.com.presentation.common.adapter.items.ErrorItemDelegateAdapter
import testgenesis.panok.com.presentation.common.adapter.items.LoadingItem
import testgenesis.panok.com.presentation.common.adapter.items.LoadingItemDelegateAdapter


class RepositoryAdapter(onRepositoryAdapterListener: OnRepositoryAdapterListener) : BaseAdapter() {


    init {
        delegateAdapters.put(AdapterKeys.REPOSITORY, RepositoryDelegateAdapter(onRepositoryAdapterListener))
        delegateAdapters.put(AdapterKeys.LOADING, LoadingItemDelegateAdapter(onRepositoryAdapterListener))
        delegateAdapters.put(AdapterKeys.ERROR, ErrorItemDelegateAdapter(onRepositoryAdapterListener))

    }


    override fun getItemViewType(position: Int): Int {
        return items[position].getViewType()
    }

    override fun addItems(items: List<ViewType>) {
        removeLoadingItem()
        super.addItems(items)

    }

    private fun removeLoadingItem() {
        if (this.items.lastOrNull() is LoadingItem)
            this.items.removeAt(this.items.size - 1)
    }

    private fun removeErrorItem() {
        if (this.items.lastOrNull() is ErrorItem)
            this.items.removeAt(this.items.size - 1)
    }

    fun startLoading() {
        removeErrorItem()
        removeLoadingItem()
        this.items.add(LoadingItem())
        notifyDataSetChanged()
    }

    fun finishLoadingError(error: ErrorUI) {
        removeLoadingItem()
        removeErrorItem()
        this.items.add(ErrorItem(error))
        notifyDataSetChanged()
    }

    interface OnRepositoryAdapterListener : ErrorItemDelegateAdapter.OnRetryLoadMore, LoadingItemDelegateAdapter.OnCancelLoadMore, OnSelectRepository


}