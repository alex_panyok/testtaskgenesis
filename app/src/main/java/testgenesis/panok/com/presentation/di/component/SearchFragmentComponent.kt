package testgenesis.panok.com.presentation.di.component

import dagger.Subcomponent
import dagger.android.AndroidInjector
import testgenesis.panok.com.presentation.main.search.SearchFragment

@Subcomponent()
interface SearchFragmentComponent : AndroidInjector<SearchFragment> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<SearchFragment>()
}