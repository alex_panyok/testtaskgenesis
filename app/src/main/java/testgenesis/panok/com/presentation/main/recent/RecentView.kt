package testgenesis.panok.com.presentation.main.recent

import testgenesis.panok.com.domain.repository.Repository
import testgenesis.panok.com.presentation.main.common.BaseMainView

interface RecentView : BaseMainView {

    fun setRecentList(list: List<Repository>)

    fun updateRepo(repository: Repository)

    fun removeRepo(repository: Repository)
}