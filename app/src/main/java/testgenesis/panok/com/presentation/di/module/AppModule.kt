package testgenesis.panok.com.presentation.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import testgenesis.panok.com.TestGenApplication
import testgenesis.panok.com.presentation.di.component.MainActivityComponent
import javax.inject.Singleton


@Module(subcomponents = arrayOf(MainActivityComponent::class), includes = arrayOf(DataModule::class, ThreadModule::class))
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: TestGenApplication): Context {
        return application.applicationContext
    }
}