package testgenesis.panok.com.presentation.main.search

import testgenesis.panok.com.domain.error.ErrorUI
import testgenesis.panok.com.domain.repository.Repository
import testgenesis.panok.com.presentation.main.common.BaseMainView


interface SearchView : BaseMainView {
    fun setRepos(list: List<Repository>)

    fun addRepos(list: List<Repository>)

    fun addReposError(error: ErrorUI)
}