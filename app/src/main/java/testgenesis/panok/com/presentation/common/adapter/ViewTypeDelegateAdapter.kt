package testgenesis.panok.com.presentation.common.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup


interface ViewTypeDelegateAdapter<in T : Any, VH : RecyclerView.ViewHolder> {
    fun onCreateViewHolder(parent: ViewGroup): VH

    fun onBindViewHolder(holder: VH, item: T)
}