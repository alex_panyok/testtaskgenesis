package testgenesis.panok.com.presentation.main.recent

import android.os.Bundle
import testgenesis.panok.com.domain.repository.GetRecentRepositoryInteractor
import testgenesis.panok.com.domain.repository.RemoveRepositoryInteractor
import testgenesis.panok.com.domain.repository.Repository
import testgenesis.panok.com.domain.repository.ViewRepositoryInteractor
import testgenesis.panok.com.presentation.main.common.BaseMainPresenter
import javax.inject.Inject

class RecentPresenter @Inject constructor(val getRecentRepositoryInteractor: GetRecentRepositoryInteractor,
                                          val viewRepositoryInteractor: ViewRepositoryInteractor,
                                          val removeRepositoryInteractor: RemoveRepositoryInteractor) : BaseMainPresenter<RecentView>() {
    override fun onStop() {
        getRecentRepositoryInteractor.unsubscribe()
        viewRepositoryInteractor.unsubscribe()
        removeRepositoryInteractor.unsubscribe()
    }


    override fun onStart(b: Bundle?) {
        getRecentRepositoryInteractor.execute(s = {
            getView()?.setRecentList(it)
        }, e = {
            getView()?.showError(transformToError(it))
        })
    }

    override fun openReposURL(repository: Repository) {
        super.openReposURL(repository)
        viewRepositoryInteractor.execute(repository.apply { viewed = true }, {
            getView()?.updateRepo(it)
        }, {
            getView()?.showError(transformToError(it))
        })
    }

    fun removeRepo(repository: Repository) {
        removeRepositoryInteractor.execute(repository, {
            getView()?.removeRepo(it)
        }, {
            getView()?.showError(transformToError(it))
        })
    }
}