package testgenesis.panok.com.presentation.di.module

import dagger.Module
import dagger.Provides
import testgenesis.panok.com.data.local.LocalDataSource
import testgenesis.panok.com.data.providers.repositories.RepositoryProviderImpl
import testgenesis.panok.com.data.providers.user.UserProviderImpl
import testgenesis.panok.com.data.remote.ApiService
import testgenesis.panok.com.domain.repository.RepositoryProvider
import testgenesis.panok.com.domain.user.UserProvider
import javax.inject.Singleton


@Module(includes = arrayOf(RemoteModule::class, DbModule::class))
class DataModule {

    @Singleton
    @Provides
    fun provideRepositoryProvider(apiService: ApiService, localDataSource: LocalDataSource): RepositoryProvider {
        return RepositoryProviderImpl(apiService, localDataSource)
    }


    @Singleton
    @Provides
    fun provideUserProvider(apiService: ApiService, localDataSource: LocalDataSource): UserProvider {
        return UserProviderImpl(apiService, localDataSource)
    }

}