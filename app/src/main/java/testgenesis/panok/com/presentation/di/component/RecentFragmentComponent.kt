package testgenesis.panok.com.presentation.di.component

import dagger.Subcomponent
import dagger.android.AndroidInjector
import testgenesis.panok.com.presentation.main.recent.RecentFragment

@Subcomponent
interface RecentFragmentComponent : AndroidInjector<RecentFragment> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<RecentFragment>()
}