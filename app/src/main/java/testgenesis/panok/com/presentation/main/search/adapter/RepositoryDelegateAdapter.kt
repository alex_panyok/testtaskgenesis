package testgenesis.panok.com.presentation.main.search.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_repository.view.*
import testgenesis.panok.com.R
import testgenesis.panok.com.domain.repository.Repository
import testgenesis.panok.com.presentation.common.adapter.ViewTypeDelegateAdapter
import testgenesis.panok.com.presentation.utils.formatStarCountString
import testgenesis.panok.com.presentation.utils.inflate
import testgenesis.panok.com.presentation.utils.setLength


class RepositoryDelegateAdapter(val onSelectRepository: OnSelectRepository) : ViewTypeDelegateAdapter<Repository, RepositoryDelegateAdapter.RepositoryHolder> {

    override fun onCreateViewHolder(parent: ViewGroup): RepositoryHolder {
        return RepositoryHolder(parent)
    }

    override fun onBindViewHolder(holder: RepositoryHolder, item: Repository) {
        holder.bind(item)
    }


    inner class RepositoryHolder(parent: ViewGroup) : RecyclerView.ViewHolder(parent.inflate(R.layout.item_repository)) {

        fun bind(repository: Repository) {
            itemView.title.setLength(30).text = repository.fullName
            itemView.desc.setLength(30).text = repository.description
            itemView.stars.text = formatStarCountString(itemView.context, repository.stars ?: 0)
            itemView.setOnClickListener { onSelectRepository.select(repository) }
        }
    }
}