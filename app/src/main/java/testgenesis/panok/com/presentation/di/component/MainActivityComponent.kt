package testgenesis.panok.com.presentation.di.component

import dagger.Subcomponent
import dagger.android.AndroidInjector
import testgenesis.panok.com.presentation.di.module.MainFragmentModule
import testgenesis.panok.com.presentation.di.module.RecentFragmentModule
import testgenesis.panok.com.presentation.di.module.SearchFragmentModule
import testgenesis.panok.com.presentation.main.MainActivity

@Subcomponent(modules = arrayOf(MainFragmentModule::class, SearchFragmentModule::class, RecentFragmentModule::class))
interface MainActivityComponent : AndroidInjector<MainActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MainActivity>()
}