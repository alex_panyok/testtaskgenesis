package testgenesis.panok.com.presentation.main.search.adapter

import testgenesis.panok.com.domain.repository.Repository

interface OnSelectRepository {

    fun select(repository: Repository)
}