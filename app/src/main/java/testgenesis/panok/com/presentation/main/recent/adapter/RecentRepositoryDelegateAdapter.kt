package testgenesis.panok.com.presentation.main.recent.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_recent_repository.view.*
import testgenesis.panok.com.R
import testgenesis.panok.com.domain.repository.Repository
import testgenesis.panok.com.presentation.common.adapter.ViewTypeDelegateAdapter
import testgenesis.panok.com.presentation.main.search.adapter.OnSelectRepository
import testgenesis.panok.com.presentation.utils.formatStarCountString
import testgenesis.panok.com.presentation.utils.inflate
import testgenesis.panok.com.presentation.utils.setLength


class RecentRepositoryDelegateAdapter(val onRecentRepositoryListener: OnRecentRepositoryListener) : ViewTypeDelegateAdapter<Repository, RecentRepositoryDelegateAdapter.RepositoryHolder> {

    override fun onCreateViewHolder(parent: ViewGroup): RepositoryHolder {
        return RepositoryHolder(parent)
    }

    override fun onBindViewHolder(holder: RepositoryHolder, item: Repository) {
        holder.bind(item)
    }


    interface OnRecentRepositoryListener : OnSelectRepository {
        fun delete(repository: Repository)
    }


    inner class RepositoryHolder(parent: ViewGroup) : RecyclerView.ViewHolder(parent.inflate(R.layout.item_recent_repository)) {

        fun bind(repository: Repository) {
            itemView.title.setLength(30).text = repository.fullName
            itemView.desc.setLength(30).text = repository.description
            itemView.stars.text = formatStarCountString(itemView.context, repository.stars ?: 0)
            itemView.viewed.visibility = if (!(repository.viewed ?: false)) View.INVISIBLE else View.VISIBLE
            itemView.setOnClickListener { onRecentRepositoryListener.select(repository) }
            itemView.remove.setOnClickListener { onRecentRepositoryListener.delete(repository) }
        }
    }
}