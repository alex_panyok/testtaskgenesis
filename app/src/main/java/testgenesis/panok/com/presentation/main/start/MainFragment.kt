package testgenesis.panok.com.presentation.main.start

import android.view.View
import kotlinx.android.synthetic.main.fragment_main.*
import testgenesis.panok.com.R
import testgenesis.panok.com.domain.error.ErrorType
import testgenesis.panok.com.domain.error.ErrorUI
import testgenesis.panok.com.domain.user.User
import testgenesis.panok.com.presentation.main.common.BaseMainFragment
import testgenesis.panok.com.presentation.utils.clearFocus
import testgenesis.panok.com.presentation.utils.toggleProgress


class MainFragment : BaseMainFragment<MainView, MainPresenter>(), MainView {
    override fun getTitle(): Int {
        return R.string.app_name
    }


    companion object {
        fun newInstance(): MainFragment {
            return MainFragment()
        }
    }


    override fun toggleProgress(show: Boolean) {
        main.toggleProgress(progressView, show, {
            presenter.onStop()
        })
    }


    override fun setUser(user: User) {
        if (!user.isEmpty()) {
            login.setText(user.login)
            password.setText("")
        }
        setUpViews(user.isEmpty())
    }

    override fun isInitial(): Boolean {
        return true
    }


    private fun setUpViews(userEmpty: Boolean) {
        login.isEnabled = userEmpty
        password.isEnabled = userEmpty
        password.visibility = if (userEmpty) View.VISIBLE else View.GONE
        searchView.visibility = if (userEmpty) View.GONE else View.VISIBLE
        bLogin.text = getString(if (userEmpty) R.string.login else R.string.logout)
    }


    override fun layoutId(): Int {
        return R.layout.fragment_main
    }

    private fun validateFields(): Boolean {
        clearFocusOnFields()
        if (login.text.isEmpty() || password.text.isEmpty()) {
            showError(ErrorUI(ErrorType.ERR_COMMON, getString(R.string.fill_in)))
            return false
        }
        return true
    }

    private fun clearFocusOnFields() {
        clearFocus(activity, login, password)
    }


    override fun initViews() {
        bLogin.setOnClickListener {
            if (presenter.isCurrentUserEmpty()) {
                if (validateFields())
                    presenter.getUser(login.text.toString(), password.text.toString())
            } else
                presenter.cleanUser()
        }
        bSearch.setOnClickListener {
            clearFocusOnFields()
            presenter.searchRepos(search.text.toString())
        }
        bRecent.setOnClickListener {
            clearFocusOnFields()
            presenter.recentRepos()
        }
    }
}