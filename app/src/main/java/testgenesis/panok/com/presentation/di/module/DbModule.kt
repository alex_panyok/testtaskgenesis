package testgenesis.panok.com.presentation.di.module

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import testgenesis.panok.com.data.local.db.AppDatabase
import javax.inject.Singleton


@Module
class DbModule {

    @Provides
    @Singleton
    fun provideDB(context: Context): AppDatabase {
        return Room.databaseBuilder(context,
                AppDatabase::class.java, "test-gen-db").build()
    }
}