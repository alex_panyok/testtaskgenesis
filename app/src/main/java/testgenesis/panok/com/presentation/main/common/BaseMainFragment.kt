@file:Suppress("UNCHECKED_CAST")

package testgenesis.panok.com.presentation.main.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import testgenesis.panok.com.R
import testgenesis.panok.com.domain.error.ErrorUI
import testgenesis.panok.com.presentation.common.BaseFragment
import testgenesis.panok.com.presentation.main.MainActivity
import testgenesis.panok.com.presentation.utils.inflate
import testgenesis.panok.com.presentation.utils.snack

abstract class BaseMainFragment<V : BaseMainView, P : BaseMainPresenter<V>> : BaseFragment<P>(), BaseMainView {

    protected var progressView: View? = null

    override fun showError(errorUI: ErrorUI) {
        val context = activity?.applicationContext
        if (context != null)
            view?.snack(errorUI.getMessage(context))
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        progressView = container?.inflate(R.layout.layout_progress)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.setView(this as V)
        presenter.setRouter(activity as MainActivity)
        presenter.getRouter()?.applyTitle(getTitle(), isInitial())
        super.onViewCreated(view, savedInstanceState)

    }

    abstract fun getTitle(): Int

    open fun isInitial(): Boolean {
        return false
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }
}