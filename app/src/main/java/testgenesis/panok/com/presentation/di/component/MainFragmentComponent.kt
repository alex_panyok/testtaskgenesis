package testgenesis.panok.com.presentation.di.component

import dagger.Subcomponent
import dagger.android.AndroidInjector
import testgenesis.panok.com.presentation.main.start.MainFragment

@Subcomponent
interface MainFragmentComponent : AndroidInjector<MainFragment> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MainFragment>()
}