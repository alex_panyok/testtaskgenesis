package testgenesis.panok.com.presentation.main.search

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_search.*
import testgenesis.panok.com.R
import testgenesis.panok.com.domain.error.ErrorType
import testgenesis.panok.com.domain.error.ErrorUI
import testgenesis.panok.com.domain.repository.Repository
import testgenesis.panok.com.presentation.main.common.BaseMainFragment
import testgenesis.panok.com.presentation.main.search.adapter.RepositoryAdapter
import testgenesis.panok.com.presentation.utils.toggleProgress
import testgenesis.panok.com.presentation.utils.views.DoublePaginatorScrollListener


class SearchFragment : BaseMainFragment<SearchView, SearchPresenter>(), SearchView, RepositoryAdapter.OnRepositoryAdapterListener {


    private val adapterRepos: RepositoryAdapter = RepositoryAdapter(this)


    private var paginateScrollListener: DoublePaginatorScrollListener? = null


    companion object {
        const val PAGINATE_OFFSET = 5
        const val QUERY = "query"
        fun newInstance(q: String): SearchFragment {
            return SearchFragment().apply { arguments = Bundle().apply { putString(QUERY, q) } }
        }
    }

    override fun toggleProgress(show: Boolean) {
        main.toggleProgress(progressView, show, {
            presenter.onStop()
        })
    }

    override fun setRepos(list: List<Repository>) {
        noResults.visibility = if (list.isEmpty()) View.VISIBLE else View.GONE
        adapterRepos.setItems(list)
        paginateScrollListener?.paginated(adapterRepos.itemCount, false)
    }

    override fun addRepos(list: List<Repository>) {
        adapterRepos.addItems(list)
        paginateScrollListener?.paginated(adapterRepos.itemCount)
        paginateScrollListener?.fullPaginate = list.isEmpty()
    }

    override fun addReposError(error: ErrorUI) {
        paginateScrollListener?.error()
        adapterRepos.finishLoadingError(error)
    }

    override fun retry() {
        adapterRepos.startLoading()
        presenter.retryNextItems()
    }

    override fun select(repository: Repository) {
        presenter.openReposURL(repository)
    }

    override fun cancel() {
        presenter.onStop()
        adapterRepos.finishLoadingError(ErrorUI(ErrorType.ERR_SERVER, getString(R.string.cancelled)))
    }

    override fun initViews() {
        val linearLayoutManager = LinearLayoutManager(activity)
        rvRepos.apply {
            layoutManager = linearLayoutManager
            adapter = adapterRepos
        }
        initPaginator(linearLayoutManager)
        rvRepos.addOnScrollListener(paginateScrollListener)
    }

    override fun getTitle(): Int {
        return R.string.search
    }

    private fun initPaginator(layoutManager: LinearLayoutManager) {
        if (paginateScrollListener == null) {
            paginateScrollListener = object : DoublePaginatorScrollListener(PAGINATE_OFFSET, layoutManager) {
                override fun paginate(page: Int) {
                    val bundle = arguments
                    if (bundle != null) {
                        adapterRepos.startLoading()
                        presenter.getNextItems(bundle.getString(QUERY), page)
                    }
                }
            }
        } else {
            paginateScrollListener?.layoutManager = layoutManager
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        paginateScrollListener?.destroy()
    }


    override fun layoutId(): Int {
        return R.layout.fragment_search
    }
}