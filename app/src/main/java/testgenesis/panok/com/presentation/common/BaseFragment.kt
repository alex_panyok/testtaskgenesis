package testgenesis.panok.com.presentation.common

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import testgenesis.panok.com.presentation.utils.inflate
import javax.inject.Inject


abstract class BaseFragment<P : BasePresenter<*, *>> : Fragment() {

    @Inject
    lateinit var presenter: P

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(layoutId())
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        AndroidSupportInjection.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        presenter.onStart(arguments)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onStop()
    }


    abstract fun initViews()


    abstract fun layoutId(): Int
}