package testgenesis.panok.com.presentation.main

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import testgenesis.panok.com.R
import testgenesis.panok.com.domain.repository.Repository
import testgenesis.panok.com.presentation.main.common.BaseMainFragment
import testgenesis.panok.com.presentation.main.recent.RecentFragment
import testgenesis.panok.com.presentation.main.search.SearchFragment
import testgenesis.panok.com.presentation.main.start.MainFragment
import javax.inject.Inject


class MainActivity : AppCompatActivity(), HasSupportFragmentInjector, MainRouter {


    @Inject lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null)
            showFragment(MainFragment.newInstance(), true)
    }


    private fun showFragment(fragment: BaseMainFragment<*, *>, initial: Boolean = false) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
        transaction.replace(container.id, fragment)
        if (!initial)
            transaction.addToBackStack(fragment.javaClass.name)
        transaction.commit()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            onBackPressed()
        return super.onOptionsItemSelected(item)
    }


    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector
    }

    override fun applyTitle(stringId: Int, initial: Boolean) {
        supportActionBar?.setDisplayHomeAsUpEnabled(!initial)
        supportActionBar?.title = getString(stringId)
    }


    override fun showSearch(q: String) {
        showFragment(SearchFragment.newInstance(q))
    }

    override fun showRecent() {
        showFragment(RecentFragment.newInstance())
    }

    override fun showRepoBrowser(repository: Repository) {
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(repository.url)))
        } catch(e: Exception) {
        }
    }


}
