package testgenesis.panok.com.presentation.di.module

import android.support.v4.app.Fragment
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap
import testgenesis.panok.com.presentation.di.component.SearchFragmentComponent
import testgenesis.panok.com.presentation.main.search.SearchFragment

@Module(subcomponents = arrayOf(SearchFragmentComponent::class))
class SearchFragmentModule {


    @Provides
    @IntoMap
    @FragmentKey(SearchFragment::class)
    fun provideSearchFragmentInjectorFactory(builder: SearchFragmentComponent.Builder): AndroidInjector.Factory<out Fragment> {
        return builder
    }
}