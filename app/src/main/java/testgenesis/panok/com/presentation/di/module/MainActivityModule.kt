package testgenesis.panok.com.presentation.di.module

import android.app.Activity
import dagger.Binds
import dagger.Module
import dagger.android.ActivityKey
import dagger.android.AndroidInjector
import dagger.multibindings.IntoMap
import testgenesis.panok.com.presentation.di.component.MainActivityComponent
import testgenesis.panok.com.presentation.di.component.MainFragmentComponent
import testgenesis.panok.com.presentation.main.MainActivity

@Module(subcomponents = arrayOf(MainFragmentComponent::class))
abstract class MainActivityModule {

    @Binds
    @IntoMap
    @ActivityKey(MainActivity::class)
    abstract fun bindMainActivityInjectorFactory(builder: MainActivityComponent.Builder): AndroidInjector.Factory<out Activity>
}