package testgenesis.panok.com.presentation.di.module

import android.support.v4.app.Fragment
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap
import testgenesis.panok.com.presentation.di.component.RecentFragmentComponent
import testgenesis.panok.com.presentation.main.recent.RecentFragment

@Module(subcomponents = arrayOf(RecentFragmentComponent::class))
abstract class RecentFragmentModule {

    @Binds
    @IntoMap
    @FragmentKey(RecentFragment::class)
    abstract fun bindRecentFragmentInjectorFactory(builder: RecentFragmentComponent.Builder): AndroidInjector.Factory<out Fragment>
}