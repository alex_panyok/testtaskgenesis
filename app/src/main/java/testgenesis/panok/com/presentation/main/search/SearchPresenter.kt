package testgenesis.panok.com.presentation.main.search

import android.os.Bundle
import testgenesis.panok.com.domain.repository.GetRemoteRepositoryInteractor
import testgenesis.panok.com.domain.repository.Repository
import testgenesis.panok.com.domain.repository.ViewRepositoryInteractor
import testgenesis.panok.com.presentation.main.common.BaseMainPresenter
import javax.inject.Inject


class SearchPresenter @Inject constructor(private val getRemoteRepositoryInteractor: GetRemoteRepositoryInteractor,
                                          private val viewRepositoryInteractor: ViewRepositoryInteractor) : BaseMainPresenter<SearchView>() {
    companion object {
        private const val FIRST_PAGE = 1
    }

    private val repos: ArrayList<Repository> = ArrayList()

    private var currentPage = FIRST_PAGE

    private var currentQuery = ""

    override fun onStart(b: Bundle?) {
        if (repos.isEmpty()) {
            if (b != null) {
                getView()?.toggleProgress(true)
                getNextItems(b.getString(SearchFragment.QUERY), FIRST_PAGE)
            }
        }
    }

    override fun openReposURL(repository: Repository) {
        super.openReposURL(repository)
        viewRepositoryInteractor.execute(repository.apply { viewed = true }, {}, {})
    }

    override fun onStop() {
        getRemoteRepositoryInteractor.unsubscribe()
    }

    private fun setRepos(list: List<Repository>) {
        getView()?.setRepos(list)
    }

    fun retryNextItems() {
        getNextItems(currentQuery, currentPage)
    }

    fun getNextItems(q: String, page: Int) {
        currentPage = page
        currentQuery = q
        getRemoteRepositoryInteractor.execute(GetRemoteRepositoryInteractor.Params(q, page = page), {
            getView()?.toggleProgress(false)
            repos.addAll(it)
            if (page == FIRST_PAGE)
                setRepos(repos)
            else
                getView()?.addRepos(it)
        }, {
            getView()?.toggleProgress(false)
            if (page == FIRST_PAGE)
                getView()?.showError(transformToError(it))
            else
                getView()?.addReposError(transformToError(it))
        })
    }


}