package testgenesis.panok.com.presentation.main.start

import testgenesis.panok.com.domain.user.User
import testgenesis.panok.com.presentation.main.common.BaseMainView

interface MainView : BaseMainView {

    fun setUser(user: User)
}