package testgenesis.panok.com.presentation.di.module

import android.support.v4.app.Fragment
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap
import testgenesis.panok.com.presentation.di.component.MainFragmentComponent
import testgenesis.panok.com.presentation.main.start.MainFragment

@Module(subcomponents = arrayOf(MainFragmentComponent::class))
abstract class MainFragmentModule {

    @Binds
    @IntoMap
    @FragmentKey(MainFragment::class)
    abstract fun bindMainFragmentInjectorFactory(builder: MainFragmentComponent.Builder): AndroidInjector.Factory<out Fragment>
}