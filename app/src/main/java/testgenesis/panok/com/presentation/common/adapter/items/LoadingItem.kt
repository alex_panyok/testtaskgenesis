package testgenesis.panok.com.presentation.common.adapter.items

import testgenesis.panok.com.presentation.common.adapter.AdapterKeys
import testgenesis.panok.com.presentation.common.adapter.ViewType


class LoadingItem : ViewType {
    override fun getViewType(): Int {
        return AdapterKeys.LOADING
    }
}