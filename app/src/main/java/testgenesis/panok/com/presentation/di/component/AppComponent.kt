package testgenesis.panok.com.presentation.di.component

import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import testgenesis.panok.com.TestGenApplication
import testgenesis.panok.com.presentation.di.module.AppModule
import testgenesis.panok.com.presentation.di.module.MainActivityModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, MainActivityModule::class, AndroidSupportInjectionModule::class))
interface AppComponent {


    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: TestGenApplication): Builder

        fun build(): AppComponent
    }

    fun inject(app: TestGenApplication)
}