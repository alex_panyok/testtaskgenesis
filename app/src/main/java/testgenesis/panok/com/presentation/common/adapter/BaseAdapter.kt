package testgenesis.panok.com.presentation.common.adapter

import android.support.v4.util.SparseArrayCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup


abstract class BaseAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    protected val items: ArrayList<ViewType> = ArrayList()

    protected var delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter<*, *>>()


    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegateAdapters.get(viewType).onCreateViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (delegateAdapters.get(getItemViewType(position)) as ViewTypeDelegateAdapter<ViewType, RecyclerView.ViewHolder>).onBindViewHolder(holder, items[position])
    }


    fun setItems(items: List<ViewType>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    open fun addItems(items: List<ViewType>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

}