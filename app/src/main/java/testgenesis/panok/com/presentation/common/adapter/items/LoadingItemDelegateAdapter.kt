package testgenesis.panok.com.presentation.common.adapter.items

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import kotlinx.android.synthetic.main.layout_progress.view.*
import testgenesis.panok.com.R
import testgenesis.panok.com.presentation.common.adapter.ViewTypeDelegateAdapter
import testgenesis.panok.com.presentation.utils.inflate


class LoadingItemDelegateAdapter(val onCancelLoadMore: OnCancelLoadMore) : ViewTypeDelegateAdapter<LoadingItem, LoadingItemDelegateAdapter.LoadingItemHolder> {

    override fun onCreateViewHolder(parent: ViewGroup): LoadingItemHolder {
        return LoadingItemHolder(parent)
    }

    override fun onBindViewHolder(holder: LoadingItemHolder, item: LoadingItem) {
        holder.bind()
    }

    interface OnCancelLoadMore {
        fun cancel()
    }


    inner class LoadingItemHolder(parent: ViewGroup) : RecyclerView.ViewHolder(parent.inflate(R.layout.item_loading_layout)) {
        fun bind() {
            itemView.cancel.setOnClickListener {
                onCancelLoadMore.cancel()
            }
        }
    }
}