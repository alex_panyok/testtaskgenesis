package testgenesis.panok.com.presentation.main.common

import testgenesis.panok.com.domain.repository.Repository
import testgenesis.panok.com.presentation.common.BasePresenter
import testgenesis.panok.com.presentation.main.MainRouter

abstract class BaseMainPresenter<V : BaseMainView> : BasePresenter<V, MainRouter>() {


    open fun openReposURL(repository: Repository) {
        getRouter()?.showRepoBrowser(repository)
    }
}