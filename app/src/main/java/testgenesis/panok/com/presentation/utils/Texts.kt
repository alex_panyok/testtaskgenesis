package testgenesis.panok.com.presentation.utils

import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import testgenesis.panok.com.R


fun formatStarCountString(context: Context, stars: Int): String {
    return String.format(context.getString(R.string.stars_format), stars)
}

fun clearFocus(context: Context?, vararg editTexts: EditText) {
    if (context != null)
        for (editText in editTexts) {
            editText.clearFocus()
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(editText.windowToken, 0)
        }
}