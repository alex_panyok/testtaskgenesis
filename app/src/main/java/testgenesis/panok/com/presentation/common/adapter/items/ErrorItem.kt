package testgenesis.panok.com.presentation.common.adapter.items

import testgenesis.panok.com.domain.error.ErrorUI
import testgenesis.panok.com.presentation.common.adapter.AdapterKeys
import testgenesis.panok.com.presentation.common.adapter.ViewType

class ErrorItem(val error: ErrorUI) : ViewType {
    override fun getViewType(): Int {
        return AdapterKeys.ERROR
    }
}