package testgenesis.panok.com.presentation.main.common

import testgenesis.panok.com.domain.error.ErrorUI

interface BaseMainView {
    fun showError(errorUI: ErrorUI)

    fun toggleProgress(show: Boolean)
}