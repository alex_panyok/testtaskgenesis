package testgenesis.panok.com.presentation.common.adapter.items

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_error_layout.view.*
import testgenesis.panok.com.R
import testgenesis.panok.com.presentation.common.adapter.ViewTypeDelegateAdapter
import testgenesis.panok.com.presentation.utils.inflate


class ErrorItemDelegateAdapter(val onRetryLoadMore: OnRetryLoadMore) : ViewTypeDelegateAdapter<ErrorItem, ErrorItemDelegateAdapter.ErrorItemHolder> {

    override fun onCreateViewHolder(parent: ViewGroup): ErrorItemHolder {
        return ErrorItemHolder(parent)
    }

    override fun onBindViewHolder(holder: ErrorItemHolder, item: ErrorItem) {
        holder.bind(item)
    }

    interface OnRetryLoadMore {
        fun retry()
    }


    inner class ErrorItemHolder(parent: ViewGroup) : RecyclerView.ViewHolder(parent.inflate(R.layout.item_error_layout)) {

        fun bind(errorItem: ErrorItem) {
            itemView.error.text = errorItem.error.getMessage(itemView.context)
            itemView.retry.setOnClickListener { onRetryLoadMore.retry() }
        }
    }
}