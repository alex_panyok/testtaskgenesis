package testgenesis.panok.com.presentation.utils.views

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView


abstract class DoublePaginatorScrollListener(private val offset: Int, var layoutManager: LinearLayoutManager?) : RecyclerView.OnScrollListener() {

    var page: Int = 1

    private var currentSize: Int = 0

    var paginating: Boolean = false

    var fullPaginate: Boolean = false

    var error: Boolean = false

    var retry: Boolean = false

    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val llManager = layoutManager
        if (llManager != null && (currentSize - llManager.findLastVisibleItemPosition()) <= offset) {
            if (!paginating && !fullPaginate && !error) {
                recyclerView?.post {
                    if (!retry) page += 2
                    paginate(page)
                }
                paginating = true
            }
        }
    }

    fun destroy() {
        layoutManager = null
        error = false
    }


    fun reset() {
        paginating = false
        fullPaginate = false
        currentSize = 0
        page = 1
    }


    fun error() {
        retry = true
        error = true
        paginating = false
    }


    fun paginated(newSize: Int, stopPaginate: Boolean = true) {
        if (stopPaginate) {
            retry = false
            paginating = false
        }
        error = false
        currentSize = newSize
    }

    abstract fun paginate(page: Int)

}