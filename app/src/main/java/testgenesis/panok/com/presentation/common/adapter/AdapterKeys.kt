package testgenesis.panok.com.presentation.common.adapter


object AdapterKeys {
    const val REPOSITORY = 1001

    const val LOADING = 1002

    const val ERROR = 1003
}