package testgenesis.panok.com.presentation.main.search.adapter

import testgenesis.panok.com.domain.repository.Repository
import testgenesis.panok.com.presentation.common.adapter.AdapterKeys
import testgenesis.panok.com.presentation.common.adapter.BaseAdapter
import testgenesis.panok.com.presentation.main.recent.adapter.RecentRepositoryDelegateAdapter


class RecentRepositoryAdapter(onRecentRepositoryListener: RecentRepositoryDelegateAdapter.OnRecentRepositoryListener) : BaseAdapter() {


    init {
        delegateAdapters.put(AdapterKeys.REPOSITORY, RecentRepositoryDelegateAdapter(onRecentRepositoryListener))
    }


    override fun getItemViewType(position: Int): Int {
        return items[position].getViewType()
    }


    fun updateRepo(repository: Repository) {
        val index = this.items.indexOf(repository)
        this.items[index] = repository
        notifyDataSetChanged()
    }

    fun removeRepo(repository: Repository) {
        this.items.remove(repository)
        notifyDataSetChanged()
    }


}