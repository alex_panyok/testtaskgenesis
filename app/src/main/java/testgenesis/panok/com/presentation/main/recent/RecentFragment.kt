package testgenesis.panok.com.presentation.main.recent

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_recent.*
import testgenesis.panok.com.R
import testgenesis.panok.com.domain.repository.Repository
import testgenesis.panok.com.presentation.main.common.BaseMainFragment
import testgenesis.panok.com.presentation.main.recent.adapter.RecentRepositoryDelegateAdapter
import testgenesis.panok.com.presentation.main.search.adapter.RecentRepositoryAdapter
import testgenesis.panok.com.presentation.utils.toggleProgress

class RecentFragment : BaseMainFragment<RecentView, RecentPresenter>(), RecentView, RecentRepositoryDelegateAdapter.OnRecentRepositoryListener {


    private val recentReposAdapter = RecentRepositoryAdapter(this)

    companion object {
        fun newInstance(): RecentFragment {
            return RecentFragment()
        }
    }

    override fun toggleProgress(show: Boolean) {
        main.toggleProgress(progressView, show, {})
    }

    override fun setRecentList(list: List<Repository>) {
        noRecent.visibility = if (list.isEmpty()) View.VISIBLE else View.GONE
        recentReposAdapter.setItems(list)
    }

    override fun initViews() {
        initRv()
    }

    override fun getTitle(): Int {
        return R.string.recent
    }

    private fun initRv() {
        rvRecent.apply {
            adapter = recentReposAdapter
            layoutManager = LinearLayoutManager(activity)
        }
    }

    override fun select(repository: Repository) {
        presenter.openReposURL(repository)
    }

    override fun layoutId(): Int {
        return R.layout.fragment_recent
    }

    override fun updateRepo(repository: Repository) {
        recentReposAdapter.updateRepo(repository)
    }

    override fun removeRepo(repository: Repository) {
        recentReposAdapter.removeRepo(repository)
    }

    override fun delete(repository: Repository) {
        presenter.removeRepo(repository)
    }
}