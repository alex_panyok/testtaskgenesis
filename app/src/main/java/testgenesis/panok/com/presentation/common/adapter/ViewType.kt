package testgenesis.panok.com.presentation.common.adapter

interface ViewType {
    fun getViewType(): Int
}