package testgenesis.panok.com.presentation.main

import testgenesis.panok.com.domain.repository.Repository


interface MainRouter {

    fun showSearch(q: String)

    fun showRecent()

    fun showRepoBrowser(repository: Repository)

    fun applyTitle(stringId: Int, initial: Boolean)
}