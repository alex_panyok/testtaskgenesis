package testgenesis.panok.com.presentation.common

import android.os.Bundle
import testgenesis.panok.com.data.remote.response.ErrorResponse
import testgenesis.panok.com.domain.error.ErrorType
import testgenesis.panok.com.domain.error.ErrorUI
import testgenesis.panok.com.domain.error.RetrofitException
import java.lang.ref.WeakReference


abstract class BasePresenter<V, R> {

    private var view: V? = null
    private var router: WeakReference<R?>? = null

    abstract fun onStart(b: Bundle?)

    abstract fun onStop()

    fun getView(): V? {
        return view
    }

    fun setView(view: V?) {
        this.view = view
    }

    fun getRouter(): R? {
        return router?.get()
    }

    fun setRouter(router: R?) {
        this.router = WeakReference(router)
    }

    fun transformToError(error: Throwable): ErrorUI {
        if (error is RetrofitException) {
            return when (error.kind) {
                RetrofitException.Kind.HTTP -> {
                    val errorResponse = parseError(error)
                    ErrorUI(ErrorType.ERR_SERVER, errorResponse?.message ?: "")

                }
                RetrofitException.Kind.NETWORK -> {
                    ErrorUI(ErrorType.ERR_NO_NETWORK)
                }
                RetrofitException.Kind.UNEXPECTED -> {
                    ErrorUI(ErrorType.ERR_UNEXPECTED)
                }
            }
        }
        return ErrorUI(ErrorType.ERR_UNEXPECTED)
    }


    private fun parseError(e: RetrofitException): ErrorResponse? {
        return e.getErrorBodyAs(ErrorResponse::class.java)
    }
}