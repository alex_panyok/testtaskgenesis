package testgenesis.panok.com.presentation.utils

import android.support.design.widget.Snackbar
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.layout_progress.view.*


fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}

fun View.snack(message: String?, length: Int = Snackbar.LENGTH_SHORT) {
    if (message != null) {
        val snack = Snackbar.make(this, message, length)
        snack.show()
    }
}

fun ViewGroup.toggleProgress(progressView: View?, show: Boolean, cancel: () -> Unit) {
    if (progressView != null) {
        progressView.cancel.setOnClickListener {
            cancel.invoke()
            toggleProgress(progressView, false, {})
        }
        if (progressView.parent == null && show)
            this.addView(progressView)
        else if (progressView.parent != null && !show)
            this.removeView(progressView)
    }
}

fun TextView.setLength(size: Int): TextView {
    this.filters = arrayOf(InputFilter.LengthFilter(size))
    return this
}