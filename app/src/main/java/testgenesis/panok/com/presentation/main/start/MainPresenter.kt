package testgenesis.panok.com.presentation.main.start

import android.os.Bundle
import testgenesis.panok.com.domain.user.CleanUserInteractor
import testgenesis.panok.com.domain.user.GetUserInteractor
import testgenesis.panok.com.domain.user.User
import testgenesis.panok.com.presentation.main.common.BaseMainPresenter
import javax.inject.Inject

class MainPresenter @Inject constructor(private val getUserInteractor: GetUserInteractor, private val cleanUserInteractor: CleanUserInteractor) : BaseMainPresenter<MainView>() {

    private var currentUser: User? = null

    override fun onStart(b: Bundle?) {
        if (currentUser == null)
            getUser()
        else
            setUser(currentUser as User)
    }

    fun getUser(login: String = "", pass: String = "") {
        getView()?.toggleProgress(true)
        getUserInteractor.execute(GetUserInteractor.Params(User.Credential(login, pass)), {
            setUser(it)
            getView()?.toggleProgress(false)
        }, {
            getView()?.toggleProgress(false)
            getView()?.showError(transformToError(it))
        })
    }

    fun cleanUser() {
        cleanUserInteractor.execute(s = {
            setUser(it)
        }, e = {
            getView()?.showError(transformToError(it))
        })
    }

    private fun setUser(user: User) {
        currentUser = user
        getView()?.setUser(user)
    }

    fun isCurrentUserEmpty(): Boolean {
        return currentUser?.isEmpty() ?: true
    }

    override fun onStop() {
        getUserInteractor.unsubscribe()
        cleanUserInteractor.unsubscribe()
    }

    fun searchRepos(q: String) {
        getRouter()?.showSearch(q)
    }

    fun recentRepos() {
        getRouter()?.showRecent()
    }
}