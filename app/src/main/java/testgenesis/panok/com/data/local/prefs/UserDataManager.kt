package testgenesis.panok.com.data.local.prefs

import android.content.Context
import com.google.gson.Gson
import testgenesis.panok.com.domain.user.User
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserDataManager @Inject constructor(context: Context, private val gson: Gson) {

    companion object {
        val USER_PREFS = "user_prefs"
        val USER = "user"
    }

    private val userPrefs = context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)


    fun setUser(user: User, auth: String): User {
        user.auth = auth
        val userJson = gson.toJson(user)
        userPrefs.edit().putString(USER, userJson).apply()
        return user
    }

    fun cleanUser(): User {
        userPrefs.edit().remove(USER).apply()
        return User.emptyUser()
    }

    fun getAuth(): String? {
        return getUser()?.auth
    }

    fun getUser(): User? {
        return gson.fromJson(userPrefs.getString(USER, ""), User::class.java)
    }
}