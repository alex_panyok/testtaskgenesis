package testgenesis.panok.com.data.remote

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.QueryMap
import testgenesis.panok.com.data.remote.response.ReposSearchResponse
import testgenesis.panok.com.domain.user.User


interface ApiService {

    @GET("/search/repositories")
    fun searchRepos(@Header("Authorization") auth: String, @QueryMap map: Map<String, String>): Observable<ReposSearchResponse>

    @GET("/user")
    fun getUser(@Header("Authorization") auth: String): Observable<User>

}