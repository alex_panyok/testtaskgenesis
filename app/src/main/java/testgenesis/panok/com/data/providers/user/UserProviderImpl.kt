package testgenesis.panok.com.data.providers.user

import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import testgenesis.panok.com.data.local.LocalDataSource
import testgenesis.panok.com.data.remote.ApiService
import testgenesis.panok.com.domain.user.User
import testgenesis.panok.com.domain.user.UserProvider
import javax.inject.Inject


class UserProviderImpl @Inject constructor(private val apiService: ApiService, private val localDataSource: LocalDataSource) : UserProvider {


    override fun cleanUser(): Single<User> {
        return localDataSource.cleanUser()
    }

    override fun getUserLocal(): Maybe<User> {
        return localDataSource.getUser()
    }

    override fun getUserRemote(auth: String): Observable<User> {
        return apiService.getUser(auth).flatMap {
            localDataSource.setUser(it, auth).toObservable()
        }
    }

}