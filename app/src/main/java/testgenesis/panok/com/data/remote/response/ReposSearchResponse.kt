package testgenesis.panok.com.data.remote.response

import com.google.gson.annotations.SerializedName
import testgenesis.panok.com.domain.repository.Repository

data class ReposSearchResponse(@SerializedName("total_count") val totalCount: Int, val items: List<Repository>)