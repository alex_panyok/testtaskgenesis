package testgenesis.panok.com.data.local

import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.toMaybe
import io.reactivex.rxkotlin.toSingle
import testgenesis.panok.com.data.local.db.AppDatabase
import testgenesis.panok.com.data.local.prefs.UserDataManager
import testgenesis.panok.com.domain.repository.Repository
import testgenesis.panok.com.domain.user.User
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class LocalDataSource @Inject constructor(private val userDataManager: UserDataManager, private val appDatabase: AppDatabase) {

    fun getUser(): Maybe<User> {
        return userDataManager.getUser().toMaybe()
    }

    fun setUser(user: User, auth: String): Single<User> {
        return userDataManager.setUser(user, auth).toSingle()
    }

    fun cleanUser(): Single<User> {
        return userDataManager.cleanUser().toSingle()
    }

    fun getAuth(): String? {
        return userDataManager.getAuth()
    }

    fun getRecentRepos(): Observable<List<Repository>> {
        return appDatabase.repoDao().getAllSortedByStarsDescending().toObservable()
    }

    fun updateRepo(repository: Repository) {
        return appDatabase.repoDao().updateOne(repository)
    }

    fun deleteRepo(repository: Repository) {
        return appDatabase.repoDao().deleteOne(repository)
    }

    fun insertRepos(list: List<Repository>) {
        appDatabase.repoDao().insertAll(list)
    }
}