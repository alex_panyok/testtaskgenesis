package testgenesis.panok.com.data.local.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import testgenesis.panok.com.data.local.db.dao.RepoDao
import testgenesis.panok.com.domain.repository.Repository

@Database(entities = arrayOf(Repository::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun repoDao(): RepoDao
}