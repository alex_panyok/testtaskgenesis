package testgenesis.panok.com.data.providers.repositories

import io.reactivex.Completable
import io.reactivex.Observable
import testgenesis.panok.com.data.local.LocalDataSource
import testgenesis.panok.com.data.remote.ApiService
import testgenesis.panok.com.domain.repository.Repository
import testgenesis.panok.com.domain.repository.RepositoryProvider
import javax.inject.Inject

class RepositoryProviderImpl @Inject constructor(private val apiService: ApiService, private val localDataSource: LocalDataSource) : RepositoryProvider {

    override fun deleteRepository(repository: Repository): Observable<Repository> {
        return Completable.fromAction { localDataSource.deleteRepo(repository) }.toObservable()
    }

    override fun updateRepository(repository: Repository): Observable<Repository> {
        return Completable.fromAction { localDataSource.updateRepo(repository) }.toObservable()
    }


    override fun getRecentRepositories(): Observable<List<Repository>> {
        return localDataSource.getRecentRepos()
    }

    override fun getRepositories(map: Map<String, String>): Observable<ArrayList<Repository>> {
        return apiService.searchRepos(localDataSource.getAuth() ?: "", map)
                .map { ArrayList(it.items) }
                .doOnNext { localDataSource.insertRepos(it) }
    }


}