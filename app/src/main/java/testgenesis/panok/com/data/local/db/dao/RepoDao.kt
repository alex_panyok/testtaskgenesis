package testgenesis.panok.com.data.local.db.dao

import android.arch.persistence.room.*
import io.reactivex.Flowable
import testgenesis.panok.com.domain.repository.Repository


@Dao
interface RepoDao {

    @Query("SELECT * FROM repository ORDER BY stars DESC")
    fun getAllSortedByStarsDescending(): Flowable<List<Repository>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(list: List<Repository>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateOne(repository: Repository)

    @Delete
    fun deleteOne(repository: Repository)

    @Delete
    fun delete(repository: Repository)
}