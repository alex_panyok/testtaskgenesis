package testgenesis.panok.com.domain.enums

enum class Order(val value: String) {
    ASC("asc"),
    DESC("desc");

    override fun toString(): String {
        return value
    }
}