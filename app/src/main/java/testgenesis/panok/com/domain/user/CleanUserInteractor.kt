package testgenesis.panok.com.domain.user

import io.reactivex.Observable
import io.reactivex.Scheduler
import testgenesis.panok.com.domain.common.Interactor
import testgenesis.panok.com.presentation.di.module.ThreadModule
import javax.inject.Inject
import javax.inject.Named

class CleanUserInteractor @Inject constructor(@Named(ThreadModule.JOB) jobScheduler: Scheduler,
                                              @Named(ThreadModule.UI) uiScheduler: Scheduler,
                                              private val userProvider: UserProvider) : Interactor<User, Unit>(jobScheduler, uiScheduler) {

    override fun buildObservable(parameter: Unit?): Observable<User> {
        return userProvider.cleanUser().toObservable()
    }
}