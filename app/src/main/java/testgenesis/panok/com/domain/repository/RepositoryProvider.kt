package testgenesis.panok.com.domain.repository

import io.reactivex.Observable


interface RepositoryProvider {

    fun getRepositories(map: Map<String, String>): Observable<ArrayList<Repository>>

    fun getRecentRepositories(): Observable<List<Repository>>

    fun updateRepository(repository: Repository): Observable<Repository>

    fun deleteRepository(repository: Repository): Observable<Repository>
}