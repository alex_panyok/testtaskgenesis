@file:Suppress("UNCHECKED_CAST")

package testgenesis.panok.com.domain.error


import io.reactivex.Observable
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.io.IOException
import java.lang.reflect.Type


class RxErrorHandlingCallAdapterFactory : CallAdapter.Factory() {
    private val original: RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()


    override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<Any, Observable<*>>? {
        return RxCallAdapterWrapper(retrofit, original.get(returnType, annotations, retrofit) as CallAdapter<Any, Any>)
    }


    private class RxCallAdapterWrapper<Any>(private val retrofit: Retrofit, private val wrapped: CallAdapter<Any, Any>?) : CallAdapter<Any, Observable<*>> {


        override fun responseType(): Type? {

            return wrapped?.responseType()
        }

        override fun adapt(call: Call<Any>): Observable<*> {
            return (wrapped?.adapt(call) as Observable<*>).onErrorResumeNext({ t: Throwable -> Observable.error(asRetrofitException(t)) })
        }


        private fun asRetrofitException(throwable: Throwable): RetrofitException {
            if (throwable is HttpException) {
                val response = throwable.response()
                return RetrofitException.httpError(response.raw().request().url().toString(), response, retrofit)
            }

            if (throwable is IOException) {
                return RetrofitException.networkError(throwable)
            }

            return RetrofitException.unexpectedError(throwable)
        }
    }

    companion object {

        fun create(): CallAdapter.Factory {
            return RxErrorHandlingCallAdapterFactory()
        }
    }
}
