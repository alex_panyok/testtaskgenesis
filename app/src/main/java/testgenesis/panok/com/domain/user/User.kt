package testgenesis.panok.com.domain.user

import com.google.gson.annotations.SerializedName


data class User(val id: String = "", val login: String = "", @SerializedName("public_repos") val publicRepos: Int = 0, var auth: String? = null) {

    companion object {
        fun emptyUser(): User {
            return User()
        }
    }

    fun isEmpty(): Boolean {
        return id.isEmpty()
    }

    class Credential(val login: String = "", val password: String = "") {

        fun isEmpty(): Boolean {
            return login.isEmpty() || password.isEmpty()
        }
    }
}