package testgenesis.panok.com.domain.common

import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable


abstract class Interactor<E, in P>(protected val jobScheduler: Scheduler, protected val uiScheduler: Scheduler) {

    private var subscription = CompositeDisposable()


    fun execute(params: P? = null, s: (p: E) -> Unit, e: (e: Throwable) -> Unit) {
        if (subscription.isDisposed)
            subscription = CompositeDisposable()
        subscription.add(buildObservable(params)
                .subscribeOn(jobScheduler)
                .observeOn(uiScheduler).subscribe(s, e))
    }

    protected abstract fun buildObservable(parameter: P?): Observable<E>

    fun unsubscribe() {
        if (!subscription.isDisposed)
            subscription.dispose()
    }
}
