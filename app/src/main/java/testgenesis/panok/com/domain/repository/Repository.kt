package testgenesis.panok.com.domain.repository

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import testgenesis.panok.com.presentation.common.adapter.AdapterKeys
import testgenesis.panok.com.presentation.common.adapter.ViewType


@Entity
class Repository : ViewType {

    @PrimaryKey
    @SerializedName("id")
    @ColumnInfo(name = "id")
    var id: Int? = 0

    @SerializedName("full_name")
    @ColumnInfo(name = "full_name")
    var fullName: String? = null


    @ColumnInfo(name = "description")
    var description: String? = null

    @SerializedName("stargazers_count")
    @ColumnInfo(name = "stars")
    var stars: Int? = 0

    @ColumnInfo(name = "viewed")
    var viewed: Boolean? = false

    @SerializedName("html_url")
    @ColumnInfo(name = "url")
    var url: String? = null


    override fun getViewType(): Int {
        return AdapterKeys.REPOSITORY
    }

    override fun toString(): String {
        return "Repository(id=$id, fullName=$fullName, description=$description, stars=$stars, viewed=$viewed)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Repository

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id ?: 0
    }


}