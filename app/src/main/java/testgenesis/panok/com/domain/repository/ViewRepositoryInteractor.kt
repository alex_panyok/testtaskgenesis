package testgenesis.panok.com.domain.repository

import io.reactivex.Observable
import io.reactivex.Scheduler
import testgenesis.panok.com.domain.common.Interactor
import testgenesis.panok.com.presentation.di.module.ThreadModule
import javax.inject.Inject
import javax.inject.Named

class ViewRepositoryInteractor @Inject constructor(@Named(ThreadModule.JOB) jobScheduler: Scheduler,
                                                   @Named(ThreadModule.UI) uiScheduler: Scheduler,
                                                   private val repositoryProvider: RepositoryProvider) : Interactor<Repository, Repository>(jobScheduler, uiScheduler) {
    override fun buildObservable(parameter: Repository?): Observable<Repository> {
        return if (parameter != null) repositoryProvider.updateRepository(parameter) else Observable.empty()
    }


}