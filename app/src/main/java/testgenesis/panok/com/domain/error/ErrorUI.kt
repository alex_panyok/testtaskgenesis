package testgenesis.panok.com.domain.error

import android.content.Context
import testgenesis.panok.com.domain.utils.toMessage

data class ErrorUI(val errorType: ErrorType, private val message: String = "") {

    fun getMessage(context: Context): String {
        return if (message.isEmpty()) {
            errorType.toMessage(context)
        } else {
            message
        }
    }

}