package testgenesis.panok.com.domain.repository

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.functions.BiFunction
import testgenesis.panok.com.domain.common.Interactor
import testgenesis.panok.com.domain.enums.Order
import testgenesis.panok.com.domain.enums.Sort
import testgenesis.panok.com.presentation.di.module.ThreadModule
import javax.inject.Inject
import javax.inject.Named


class GetRemoteRepositoryInteractor @Inject constructor(@Named(ThreadModule.JOB) jobScheduler: Scheduler,
                                                        @Named(ThreadModule.UI) uiScheduler: Scheduler,
                                                        private val repositoryProvider: RepositoryProvider,
                                                        private val gson: Gson) : Interactor<List<Repository>, GetRemoteRepositoryInteractor.Params>(jobScheduler, uiScheduler) {


    override fun buildObservable(parameter: Params?): Observable<List<Repository>> {
        return if (parameter != null) {
            val map: HashMap<String, String> = gson.fromJson(gson.toJson(parameter), object : TypeToken<HashMap<String, String>>() {}.type)
            val mapNext = HashMap<String, String>(map).apply {
                put("page", parameter.page.plus(1).toString())
            }
            Observable.zip(
                    repositoryProvider.getRepositories(map).subscribeOn(jobScheduler),
                    repositoryProvider.getRepositories(mapNext).subscribeOn(jobScheduler), BiFunction { list, listNext ->
                list.apply {
                    addAll(listNext)
                }
            })
        } else {
            Observable.empty<List<Repository>>()
        }
    }


    class Params(val q: String,
                 val sort: String = Sort.STAR.value,
                 val order: String = Order.DESC.value,
                 @SerializedName("page") val page: Int = 1,
                 @SerializedName("per_page") val perPage: Int = 15)

}