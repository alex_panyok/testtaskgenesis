package testgenesis.panok.com.domain.repository

import io.reactivex.Observable
import io.reactivex.Scheduler
import testgenesis.panok.com.domain.common.Interactor
import testgenesis.panok.com.presentation.di.module.ThreadModule
import javax.inject.Inject
import javax.inject.Named


class GetRecentRepositoryInteractor @Inject constructor(@Named(ThreadModule.JOB) jobScheduler: Scheduler,
                                                        @Named(ThreadModule.UI) uiScheduler: Scheduler,
                                                        private val repositoryProvider: RepositoryProvider) : Interactor<List<Repository>, Unit>(jobScheduler, uiScheduler) {


    override fun buildObservable(parameter: Unit?): Observable<List<Repository>> {
        return repositoryProvider.getRecentRepositories()
    }


}