package testgenesis.panok.com.domain.user

import io.reactivex.Observable
import io.reactivex.Scheduler
import testgenesis.panok.com.domain.common.Interactor
import testgenesis.panok.com.domain.utils.toAuth
import testgenesis.panok.com.presentation.di.module.ThreadModule
import javax.inject.Inject
import javax.inject.Named


class GetUserInteractor @Inject constructor(@Named(ThreadModule.JOB) jobScheduler: Scheduler,
                                            @Named(ThreadModule.UI) uiScheduler: Scheduler,
                                            private val userProvider: UserProvider) : Interactor<User, GetUserInteractor.Params>(jobScheduler, uiScheduler) {

    override fun buildObservable(parameter: GetUserInteractor.Params?): Observable<User> {
        return if (parameter == null || parameter.credential.isEmpty())
            userProvider.getUserLocal().switchIfEmpty(
                    Observable.just(User.emptyUser()).firstElement()
            ).toObservable()
        else
            userProvider.getUserRemote(parameter.credential.toAuth())
    }


    class Params(val credential: User.Credential = User.Credential())

}