package testgenesis.panok.com.domain.error


enum class ErrorType {
    ERR_NO_NETWORK,
    ERR_SERVER,
    ERR_UNEXPECTED,
    ERR_COMMON
}