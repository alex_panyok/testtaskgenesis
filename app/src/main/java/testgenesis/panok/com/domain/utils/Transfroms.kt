package testgenesis.panok.com.domain.utils

import android.content.Context
import android.util.Base64
import testgenesis.panok.com.R
import testgenesis.panok.com.domain.error.ErrorType
import testgenesis.panok.com.domain.user.User


fun User.Credential.toAuth(): String {
    val credentials: String = login + ":" + password
    return "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
}

fun ErrorType.toMessage(context: Context): String {
    return when (this) {
        ErrorType.ERR_NO_NETWORK -> {
            context.resources.getString(R.string.no_network_error)
        }
        ErrorType.ERR_UNEXPECTED -> {
            context.resources.getString(R.string.unexpected_error)
        }
        ErrorType.ERR_SERVER -> {
            context.resources.getString(R.string.server_error)
        }
        ErrorType.ERR_COMMON -> {
            context.resources.getString(R.string.server_error)
        }
    }
}