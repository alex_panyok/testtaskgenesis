package testgenesis.panok.com.domain.enums

enum class Sort(val value: String) {
    STAR("stars");

    override fun toString(): String {
        return value
    }
}