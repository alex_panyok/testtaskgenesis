package testgenesis.panok.com.domain.user

import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single


interface UserProvider {

    fun getUserLocal(): Maybe<User>

    fun getUserRemote(auth: String): Observable<User>

    fun cleanUser(): Single<User>
}